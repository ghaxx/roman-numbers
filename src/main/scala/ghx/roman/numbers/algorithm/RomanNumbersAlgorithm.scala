package ghx.roman.numbers.algorithm

class RomanNumbersAlgorithm() {
  private val romanLiterals = Map(
    1 -> "I",
    4 -> "IV",
    5 -> "V",
    9 -> "IX",
    10 -> "X",
    40 -> "XL",
    50 -> "L",
    90 -> "XC",
    100 -> "C",
    400 -> "CD",
    500 -> "D",
    900 -> "CM",
    1000 -> "M"
  )

  def translate(latinNumber: Int): String = {
    translate(latinNumber, romanLiterals.toList.sortBy(_._1).reverse)
  }

  private def translate(latinNumber: Int, romanLiterals: List[(Int, String)]): String = {
    romanLiterals match {
      case (value, code) :: tail if value <= latinNumber => code + translate(latinNumber - value, romanLiterals)
      case (value, code) :: tail if value > latinNumber => translate(latinNumber, tail)
      case Nil => ""
    }
  }

  def translate(romanNumber: String): Int = {
    translate(romanNumber, romanLiterals.toList.sortBy(_._1).reverse)
  }

  private def translate(romanNumber: String, romanLiterals: List[(Int, String)]): Int = {
    romanLiterals match {
      case (value, code) :: tail if romanNumber.startsWith(code) => value + translate(romanNumber.drop(code.length), romanLiterals)
      case (value, code) :: tail => translate(romanNumber, tail)
      case Nil => 0
    }
  }

}
