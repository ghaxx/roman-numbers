package ghx.roman.numbers.algorithm

class RomanNumbersAlgorithm2() {
  private val romanLiterals = Map(
    1 -> 'I',
    5 -> 'V',
    10 -> 'X',
    50 -> 'L',
    100 -> 'C',
    500 -> 'D',
    1000 -> 'M'
  )

  def translate(latinNumber: Int): String = {
    translate(latinNumber, romanLiterals.toList.sortBy(_._1).reverse)
  }

  private def translate(latinNumber: Int, romanLiterals: List[(Int, Char)]): String = {
    ???
  }

  def translate(romanNumber: String): Int = {
    val literals = romanLiterals.toList.sortBy(_._1).map{case(n, c) ⇒ (c, n)}.reverse.toMap
    val minLiteralValue = literals.minBy(_._2)._2
    translate(romanNumber.reverse.toList, literals, minLiteralValue)
  }

  def translateIteratively(romanNumber: String): Int = {
    val literals = romanLiterals.toList.sortBy(_._1).map{case(n, c) ⇒ (c, n)}.reverse.toMap
    translateIteratively(romanNumber.toList, literals)
  }

  private def translate(romanNumber: List[Char], romanLiterals: Map[Char, Int], maxLiteralValue: Int): Int = {
    romanNumber match {
      case head :: tail if romanLiterals(head) >= maxLiteralValue ⇒ romanLiterals(head) + translate(tail, romanLiterals, romanLiterals(head))
      case head :: tail if romanLiterals(head) < maxLiteralValue ⇒ -romanLiterals(head) + translate(tail, romanLiterals, maxLiteralValue)
      case Nil ⇒ 0
    }
  }

  private def translateIteratively(romanNumber: List[Char], romanLiterals: Map[Char, Int]): Int = {
    var maxLiteralValue = romanLiterals.minBy(_._2)._2
    romanNumber.foldRight(0) { (char, memo) =>
        val charValue = if (romanLiterals(char) >= maxLiteralValue) romanLiterals(char) else -romanLiterals(char)
        maxLiteralValue = math.max(charValue, maxLiteralValue)
        memo + charValue
    }
  }

}
