package ghx.roman.numbers.algorithm

import org.scalatest.{FlatSpec, Matchers}

class RomanNumbersAlgorithmTest extends FlatSpec with Matchers {

  import TestData._

  it should "translate latin to roman" in {
    val algorithm = new RomanNumbersAlgorithm
    for {(number, code) <- numbers} {
      algorithm.translate(number) shouldBe code
    }
  }

  it should "translate roman to latin" in {
    val algorithm = new RomanNumbersAlgorithm
    for {(number, code) <- numbers} {
      algorithm.translate(code) shouldBe number
    }
  }

}
