package ghx.roman.numbers.algorithm

object TestData {
  val numbers = Map(
    2 -> "II",
    3 -> "III",
    4 -> "IV",
    13 -> "XIII",
    29 -> "XXIX",
    43 -> "XLIII",
    65 -> "LXV",
    494 -> "CDXCIV",
    944 -> "CMXLIV",
    994 -> "CMXCIV",
    1492 -> "MCDXCII",
    1994 -> "MCMXCIV",
    2015 -> "MMXV"
  )
}
